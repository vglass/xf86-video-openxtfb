/**
 * OpenXT Framebuffer ("openxtfb") Driver for X11
 * Authors: Kyle J. Temkin <temkink@ainfosec.com>
 *
 * RandR support extension
 *
 * Based on the fbdev driver, by:
 * Authors:
 *       Alan Hourihane, <alanh@fairlite.demon.co.uk>
 *       Michel Dänzer, <michel@tungstengraphics.com>
 */

#ifndef __RANDR_H__
#define __RANDR_H__

//Include the files necessary for RandR support.
#include "xf86Crtc.h"
#include "xf86Modes.h"

/**
 * Prepares the framebuffer for modesetting via RandR by
 * creating a virtual CRTC for the OpenXT framebuffer.
 */
Bool openxtfbPrepareForRandR(ScrnInfoPtr pScrn, openxtfbPtr oxtPtr);



#endif
