/**
 * OpenXT Framebuffer ("openxtfb") Driver for X11
 * Authors: Kyle J. Temkin <temkink@ainfosec.com>
 *
 * RandR support extension.
 *
 * There's very little actual code in here-- it's
 * mostly boilerplate. Coincidentally, there's very 
 * little hardware to set up for a virtual driver.
 *
 * Based on OpenXT's xenfb2 RandR extension, by:
 *       Julian Pidancet, <julian.pidancet@citrix.com>
 *
 * Based on the fbdev driver, by:
 *       Alan Hourihane, <alanh@fairlite.demon.co.uk>
 *       Michel Dänzer, <michel@tungstengraphics.com>
 */


#include "openxtfb.h"

/**
 * Handles a resize of the OpenXT framebuffer's "virtual CRTC",
 * effectively resizing the OpenXT framebuffer.
 */ 
static Bool openxtfbCrtcResize(ScrnInfoPtr pScrn, int width, int height)
{
    //Allow resizing of the display, as long as the new width and height
    //fall within our accepted limits. (RandR should also help to enforce
    //this!)
    return (width <= pScrn->virtualX) && (height <= pScrn->virtualY);
}


/**
 * The configuration function(s) for our virtual CRTC-- these
 * allow manipulation of our virtual CRTC.
 */
static const xf86CrtcConfigFuncsRec openxtfbCrtcConfigFuncs = {
    .resize = openxtfbCrtcResize
};


/**
 * Set the Display Power Management Signaling mode for the
 * virtual CRTC. Does nothing, as DPMS is handled by the remote
 * display handler.
 */
static void openxtfbCrtcDpms(xf86CrtcPtr crtc, int mode)
{
    (void) crtc;
    (void) mode;
}

/**
 * Locks access to the CRTC prior to modesetting.
 *
 * This function is for the benefit of DRI devices; our modesetting
 * is as atomic as it can be inside of the fbdev kernel module.
 */
static Bool openxtfbCrtcLock(xf86CrtcPtr crtc)
{
    (void) crtc;
    return FALSE;
}


/**
 * Hook function which allows us to manipulate timing before
 * they're passed to the hardware. In our case, we don't actually
 * have a CRTC, so there's nothing for us to do.
 */
static Bool openxtfbCrtcModeFixup(xf86CrtcPtr crtc, DisplayModePtr mode,
                   DisplayModePtr adjusted_mode)
{
    (void) mode;
    return TRUE;
}


/**
 * Prepares the virtual CRTC for a modeset.
 * Again, no CRTC = nothing to do.
 */
static void openxtfbCrtcPrepare(xf86CrtcPtr crtc)
{
    (void) crtc;
}

/**
 * Commit a mode change to the hardware.
 * We have no real hardware, and thus nothing to do.
 */
static void openxtfbCrtcCommit(xf86CrtcPtr crtc)
{
        (void) crtc;
}


/**
 * Sets the gamma adjustment for the given CRTC.
 * Again, nothing to do.
 */
static void openxtfbCrtcGammaSet(xf86CrtcPtr crtc, CARD16 *red,
                  CARD16 *green, CARD16 *blue, int size)
{
        (void) crtc;
        (void) red;
        (void) green;
        (void) blue;
        (void) size;
}


/**
 * Allocates any per-output storage is necessary for the given display.
 *
 * In our case, our displays are directly equivalent to the framebuffer, so
 * no shadow buffer is alloated.
 */
static void * openxtfbCrtcShadowAllocate(xf86CrtcPtr crtc, int width, int height)
{
        (void) crtc;
        (void) width;
        (void) height;

        return NULL;
}

/**
 * T743 down the driver-specific portions of the CRTC.
 * The nice thing about a pure virtual driver is that we don't need any!
 */
static void openxtfbCrtcDestroy(xf86CrtcPtr crtc)
{
        (void) crtc;
}


/**
 * Handles a modeset for the virtual CRTC. In this case, we pass
 * the modeset directly to the underlying framebuffer.
 */
static void openxtfbCrtcModeSet(xf86CrtcPtr crtc, DisplayModePtr mode,
    DisplayModePtr adjusted_mode, int x, int y)
{
    ScrnInfoPtr pScrn = crtc->scrn;
    openxtfbPtr oxtPtr = (openxtfbPtr)pScrn->driverPrivate;

    (void) mode;

    //In addition to accepted our adjusted mode, use the
    //openxtfb fixed modesetting information.
    pScrn->virtualX     = oxtPtr->virtualX;
    pScrn->virtualY     = oxtPtr->virtualY;
    pScrn->displayWidth = oxtPtr->virtualX;

    //Pass the desired mode directly to the underlying framebuffer.
    fbdevHWModeInit(pScrn, adjusted_mode);

    //... and adjust the X11 frame to match the newly set mode.
    pScrn->AdjustFrame(ADJUST_FRAME_ARGS(pScrn, x, y));
}


/**
 * Core CRTC manipulation functions for our emulated CRT
 * controller. Many of these don't do anything, as they don't
 * make sense in an emulated display setting.
 */
static const xf86CrtcFuncsRec openxtfbCrtcFuncs = {
        .dpms = openxtfbCrtcDpms,
        .save = NULL,
        .restore = NULL,
        .lock = openxtfbCrtcLock,
        .unlock = NULL,
        .mode_fixup = openxtfbCrtcModeFixup,
        .prepare = openxtfbCrtcPrepare,
        .mode_set = openxtfbCrtcModeSet,
        .commit = openxtfbCrtcCommit,
        .gamma_set = openxtfbCrtcGammaSet,
        .shadow_allocate = openxtfbCrtcShadowAllocate,
        .shadow_create = NULL,
        .shadow_destroy = NULL,

        //TODO: Should these be our hardware cursor functions?
        .set_cursor_colors = NULL,
        .set_cursor_position = NULL,
        .show_cursor = NULL,
        .hide_cursor = NULL,
        .load_cursor_argb = NULL,

        .destroy = openxtfbCrtcDestroy
};


static void openxtfbOutputCreateResources(xf86OutputPtr output)
{
      (void) output;
}


/**
 * Called to turn the output on or off. We don't directly support
 * this, currently-- power savings are handled by the Display Handler.
 */
static void openxtfbOutputDpms(xf86OutputPtr output, int mode)
{
      (void) output;
      (void) mode;
}


/**
 * Determines if a mode is valid for a given output.
 *
 * Per the X instructions, this doesn't take into account limitations of
 * e.g. our virtual CRTC, which limits resolution.
 *
 * @param output The output for which the mode should be checked.
 * @parma mode The mode to be checked.
 */ 
static int openxtfbOutputModeValid(xf86OutputPtr output, DisplayModePtr mode)
{
    //Our outputs are purely virtual, and don't actually do anything--
    //so we rely only on our base mode restriction, which we enforce
    //in our virtual CRTC.
    return MODE_OK;
}


/**
 * Fixes up a provided mode so it works with the given output.
 *
 * @param output The output for which we're fixing the mode.
 * @param mode The original mode.
 * @param mode A copy of the mode, to be modified, if necessary.
 */
static Bool openxtfbOutputModeFixup(xf86OutputPtr output, DisplayModePtr mode,
                     DisplayModePtr adjusted_mode)
{
    (void) output;
    (void) mode;
    (void) adjusted_mode;

    //All acceptable modes work with our outputs-- so this is a no-op.
    //(We'll never be passed a mode that exceeds our virtual size.)
    return TRUE;
}


/**
 * Called before a mode-set occurs. Not used.
 */
static void openxtfbOutputPrepare(xf86OutputPtr output)
{
    (void) output;
}

/**
 * Called after a mode-set has occurred in X; typically used
 * to commit the mode changes to the actual device. Not used.
 */
static void openxtfbOutputCommit(xf86OutputPtr output)
{
    (void) output;
}


/**
 * Callback for setting up a video mode after fixups have been made.
 * This is only called while the output is disabled.
 */
static void openxtfbOutputModeSet(xf86OutputPtr output, DisplayModePtr mode,
                   DisplayModePtr adjusted_mode)
{
    (void) output;
    (void) mode;
    (void) adjusted_mode;
}


/**
 * Returns whether the output is connected or disconnected.
 * In our case, we report that the virtual output is always connected.
 */
static xf86OutputStatus openxtfbOutputDetect(xf86OutputPtr output)
{
    (void) output;
    return XF86OutputStatusConnected;
}


/**
 * Cleans up any per-output information that has been allocated by the driver.
 */
static void openxtfbOutputDestroy(xf86OutputPtr output)
{
    (void) output;
    //We don't allocate any information, so we have nothing to clean up.
}


#ifdef RANDR_12_INTERFACE
/**
 * Callback that is executed when any output properties are changed.
 */
static Bool openxtfbOutputSetProperty(xf86OutputPtr output, Atom property,
    RRPropertyValuePtr value)
{
    (void) output;
    (void) property;
    (void) value;

    //We currently don't support any properties!
    return FALSE;
}
#endif


/**
 * Registers an Xorg mode as one of the modes provided by a given output.
 *
 * @param output The output whose modes should be adjusted.
 * @param mode The mode to be added.
 */
static void openxtfbOutputAddProbedMode(xf86OutputPtr output, DisplayModePtr mode)
{
    DisplayModePtr lastMode = NULL;

    if(!mode)
      return;

    //Case 1:
    //The list is empty, so this becomes our new head.
    if(!output->probed_modes) {
         output->probed_modes = mode;
         mode->next = NULL;
         mode->prev = NULL;
         return;
    }

    //Case 2:
    //There are elements in the list; suffix our elemnt.
    else {
        //Find the last element in our list...
        for(lastMode = output->probed_modes;
            lastMode->next; lastMode = lastMode->next);

        //... and append our mode to it.
        lastMode->next = mode;
        mode->prev = lastMode;
        mode->next = NULL;
    }
}


/**
 * Automatically applies a resolution-based name to the provided mode.
 * Useful for dynamically created modes.
 *
 * @param mode The mode to be named.
 */
static DisplayModePtr openxtfbNameMode(DisplayModePtr mode)
{
    if((mode->HDisplay < 0) || (mode->VDisplay < 0))
      return;

    //If we already have a name, release it.
    if(mode->name)
      free(mode->name);

    //Finally, set a descriptive name for the mode.
    mode->name = malloc(sizeof("AAAAAxBBBBB"));
    sprintf(mode->name, "%dx%d", mode->HDisplay % 100000, mode->VDisplay % 100000);

}


/**
 * Creates a new mode for use by the OpenXT framebuffer.
 *
 * @param pScrn The screen for which the mode will be created.
 * @param width The X resolution of the new mode.
 * @param height The Y resolution of the new mode.
 *
 */
static DisplayModePtr openxtfbCreateMode(ScrnInfoPtr pScrn, int width, int height)
{
    DisplayModePtr newMode;

    //Create a copy of the framebuffer's built-in mode, and use this
    //to as a baseline for our new mode.
    newMode = xf86DuplicateMode(fbdevHWGetBuildinMode(pScrn));

    //If we couldn't create our mode, fail out.
    if(!newMode)
        return NULL;

    //Set the width and height. We set quite a few ancillary properties,
    //as well, even though X doesn't use them, so our modes match fbdev's.
    //
    //This makes comparing modes easier.
    newMode->HDisplay = newMode->HSyncStart = width;
    newMode->HSyncEnd = newMode->HTotal     = width;
    newMode->VDisplay = newMode->VSyncStart = height;
    newMode->VSyncEnd = newMode->VTotal     = height;

    //Give the mode a compelling name.
    openxtfbNameMode(newMode);

    newMode->status = MODE_OK;
    return newMode;
}


/**
 * Queries the OpenXT framebuffer, and returns its "desired" mode--
 * that is, the mode determined to best match the Display Handler.
 *
 * @param pScrn The relevant Xorg screen.
 */
static DisplayModePtr openxtfbGetDesiredMode(ScrnInfoPtr pScrn)
{
    int rc;
    int width, height;
    static struct openxtfb_size_hint size_hint;
    openxtfbPtr oxtPtr = (openxtfbPtr)pScrn->driverPrivate;

    //Ask the OpenXT framebuffer for information regarding its geometry...
    rc = ioctl(oxtPtr->fd, OPENXTFB_IOCTL_GET_SIZE_HINT, &size_hint);

    //If we couldn't read the size information from openxtfb,
    //return NULL.
    if(rc) {
      xf86DrvMsg(pScrn->scrnIndex, X_ERROR, "Failed to get size hint (%s).\n",
          strerror(errno));
      return NULL;
    }

    //Compute the closest we can get to the desired mode.
    width = min(size_hint.width, pScrn->virtualX);
    height = min(size_hint.height, pScrn->virtualY);

    //Return a newly-created mode with the appropriate resolution.
    return openxtfbCreateMode(pScrn, width, height);
}



/**
 * Returns a full list of modes _suggested_ for the current display.
 *
 * Using RandR 1.2, we actually support the addition and use of any mode
 * that will fit within our framebuffer, so these are more for the sake
 * of suggestion.
 *
 * @param output The output for which modes are being probed.
 */
static DisplayModePtr openxtfbOutputGetModes (xf86OutputPtr output)
{
    DisplayModePtr currentMode, desiredMode;

    ScrnInfoPtr pScrn = output->scrn;
    openxtfbPtr oxtPtr = (openxtfbPtr)pScrn->driverPrivate;

    //Clear all existing modes from out mode list...
    while(output->probed_modes)
      xf86DeleteMode(&output->probed_modes, output->probed_modes);

    //... add the display handler's current mode as a valid mode.
    desiredMode = openxtfbGetDesiredMode(pScrn);
    if (!desiredMode)
      return NULL;
    openxtfbOutputAddProbedMode(output, desiredMode);

    //... and the framebuffer's current mode.
    currentMode = xf86DuplicateMode(pScrn->currentMode);
    openxtfbNameMode(currentMode);
    openxtfbOutputAddProbedMode(output, currentMode);

    //Finally, set the CRTC's desired Mode to match the display handler's
    //desired mode.
    oxtPtr->crtc->desiredMode = *desiredMode;
    
    //... and return our list of probed modes.
    return output->probed_modes;
}


/**
 * Virtual output manipulation functions. Many of these don't
 * do anything, as like the CRTC functions, they don't make sense
 * for an emulated device.
 */
static const xf86OutputFuncsRec openxtfbOutputFuncs = {
        .create_resources = openxtfbOutputCreateResources,
        .dpms = openxtfbOutputDpms,
        .save = NULL,
        .restore = NULL,
        .mode_valid = openxtfbOutputModeValid,
        .mode_fixup = openxtfbOutputModeFixup,
        .prepare = openxtfbOutputPrepare,
        .commit = openxtfbOutputCommit,
        .mode_set = openxtfbOutputModeSet,
        .detect = openxtfbOutputDetect,
        .get_modes = openxtfbOutputGetModes,
#ifdef RANDR_12_INTERFACE
        .set_property = openxtfbOutputSetProperty,
#endif
        .destroy = openxtfbOutputDestroy
};

/**
 * Prepares the framebuffer for modesetting via RandR by
 * creating a virtual CRTC for the OpenXT framebuffer.
 */
Bool openxtfbPrepareForRandR(ScrnInfoPtr pScrn, openxtfbPtr oxtPtr) 
{
    xf86CrtcPtr crtc;
    xf86OutputPtr output;

    //Create our monitor; and allow resolutions up to our virtual max.
    xf86CrtcConfigInit(pScrn, &openxtfbCrtcConfigFuncs);
    xf86CrtcSetSizeRange(pScrn, 64, 64, oxtPtr->virtualX, oxtPtr->virtualY);


    //Create the virtual CRTC controller and output for this device.
    //These are required to configure the device using RandR.
    oxtPtr->crtc = xf86CrtcCreate(pScrn,   &openxtfbCrtcFuncs);
    output = xf86OutputCreate(pScrn, &openxtfbOutputFuncs, "virtual");
    xf86OutputUseScreenMonitor(output, FALSE);

    //Only support a single CRTC, for now, as we only have a single
    //virtual framebuffer/backing store. It's possible to implement
    //multi-monitor directly in X, but we'll prefer to do that at
    //the DRM level in the guest tools.
    output->possible_crtcs  = 1;
    output->possible_clones = 0;

    //Finally, set everything up!
    if (!xf86InitialConfiguration(pScrn, TRUE)) {
            xf86DrvMsg(pScrn->scrnIndex, X_ERROR, "Initial CRTC configuration failed!\n");
            return FALSE;
    }

    return TRUE;
}
